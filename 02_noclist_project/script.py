#-*- encoding:utf-8 -*-

#    ------------------------------------------------------------------------
#    Author: Ignacio de Abelleyra
#    created: 29/11/2019
#    last modified: 30/11/2019
#    Modified by: Ignacio de Abelleyra
#    Python Version: 3.7
#    ------------------------------------------------------------------------
#    Notes: Run this code within a virutal Environment.
#    Step1: virtualenv -p python3 <myvenv_name>
#    Step2: source myvenv/bin/activate
#    ------------------------------------------------------------------------


from __future__ import print_function
import urllib.request
import urllib.parse
import requests
import hashlib
import inspect
import pprint
import json
import sys


# ----------------------------------------------------------------------------------------------------------------------
# Variables
# ----------------------------------------------------------------------------------------------------------------------

url = 'http://localhost:8888'

endpointAuth = '/auth'
endpointUsers = '/users'

urlAuth = url + endpointAuth
urlClients = url + endpointUsers

key = ''
count = 0


# ----------------------------------------------------------------------------------------------------------------------
# Function: Get authentication Token.
# ----------------------------------------------------------------------------------------------------------------------

def requestToken():
  tokenHeader = 'Badsec-Authentication-Token'
  authToken = ''
  response = ''
  count = 0
  while count != 3:
    count += 1
    key = True
    try:
      # Hit /auth endpoint.
      response = requests.get(urlAuth)
    except requests.exceptions.RequestException as e:
      print(" [ ERROR ] - [ TOKEN ] - Exception found while getting a response from the API: \n"
            " Exception: "
            + str(e), file=sys.stderr)
      key = False
    if key:
      try:
        if response.status_code == 200:
          authToken = response.headers[tokenHeader]
          key = True
          break
        else:
          print(" [ ERROR ] - [ TOKEN ] - Received status code "
                + str(response.status_code ) + " from the API \n", file=sys.stderr)
          key = False
      except AttributeError as error:
        print(" [ ERROR ] - [ TOKEN ] - response is not a class\n", file=sys.stderr)
        key = False
    print(" ---------------------------\n")
  return key, authToken


# ----------------------------------------------------------------------------------------------------------------------
# Function: Get List of User ID's
# ----------------------------------------------------------------------------------------------------------------------

def getUsersID(authToken):
  checksumHeader = 'X-Request-Checksum'
  string = authToken + endpointUsers
  checksum = hash_string(string)
  response = ''
  idList = ''
  count = 0
  while count != 3:
    count += 1
    key = 'True'
    try:
      # Hit /users endpoint.
      response = requests.get(urlClients, headers={checksumHeader: checksum})
    except requests.exceptions.RequestException as e:
      print(" [ ERROR ] - [ ID List ] - Exception found while getting a response from the API\n"
            " Exception: "
            + str(e), file=sys.stderr)
      key = False
    if key :
      try:
        response.status_code == 203
        if response.status_code == 200:
          idList = response.text
          key = True
          break
        else:
          print(" [ ERROR ] - [ ID List ] - Received status code "
                + str(response.status_code) + " from the API \n", file = sys.stderr)
          key = False
      except AttributeError as error:
        print(" [ ERROR ] - [ ID List ] - response is not a class\n", file=sys.stderr)
        key = False
    print(" ---------------------------\n")
  return key, idList


# ----------------------------------------------------------------------------------------------------------------------
# Convert Authentication String to SHA-256
# ----------------------------------------------------------------------------------------------------------------------

def hash_string(myString):
    return hashlib.sha256(myString.encode('utf-8')).hexdigest()


# ----------------------------------------------------------------------------------------------------------------------
# Convert user's ID list from string to Json.
# ----------------------------------------------------------------------------------------------------------------------

def string2json(string):
  # Conversts string to list and removes line breaks.
  newList = string.splitlines()
  # Converts list to json.
  newJson = json.dumps(newList)
  return newJson


# ----------------------------------------------------------------------------------------------------------------------
# The Main function will try to run this function three times.
# ----------------------------------------------------------------------------------------------------------------------

def main(a):
  if a > 1:
    print("\n ##################################################################################\n"
          " [ ATTEMPT #" + str(a) + " ] -  Running MAIN Function. \n"
          " ##################################################################################\n", file=sys.stderr)
  tokenKey, token = requestToken()

  if tokenKey:
    # Get users ID listdd.
    usersKey, list = getUsersID(token)
    if usersKey == True:
      # Convert ID list to Json.
      result = string2json(list)
      print(result)
      return True
    else:
      print(" ----------------------------------------------------------------------------------\n"
            " [ DEBUG ] - Failed while getting the users's key list from the API.\n"
      " ----------------------------------------------------------------------------------\n", file = sys.stderr)
      return False
  else:
    print(" ----------------------------------------------------------------------------------\n"
          " [ DEBUG ] - Failed while getting the Token Key from the API.\n"
          " ----------------------------------------------------------------------------------\n", file=sys.stderr)
    return False


# ----------------------------------------------------------------------------------------------------------------------
# Execute
#-----------------------------------------------------------------------------------------------------------------------
while count <= 3 and key != True:
  count += 1
  key = main(count)

  if key == True and count >= 2:
    print("\n ####################################################################################\n"
          " [ Result ] - [ Successful ] - Successfully retrieved the list of user ID\'s\n"
          " ####################################################################################\n", file=sys.stderr)
  if count == 3 and key == False:
    print("\n ####################################################################################\n"
          " [ Result ] - [ Failed ] - Failed to get the list of user ID\'s\n"
          " ####################################################################################\n", file=sys.stderr)
    sys.exit(1)
  if key == True:
    sys.exit(0)
