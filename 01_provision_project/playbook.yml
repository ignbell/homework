---
#    ------------------------------------------------------------------------
#    Author: Ignacio de Abelleyra
#    created: 28/11/2019
#    last modified: 28/11/2019
#    Modified by: Ignacio de Abelleyra
#    Python Version: 3.7
#    ------------------------------------------------------------------------
- hosts: all
  become: True
  any_errors_fatal: true
  tasks:
    - include: tasks/deps.yml
    - name: install ufw 
      yum: 
        name: ufw
        state: latest
        update_cache: yes
    - name: Allow everything and enable UFW
      ufw:
       state: enabled
       policy: deny
    - name: Set logging
      ufw:
        logging: 'on'
    - ufw:
        rule: reject
        port: auth
        log: yes
    - ufw: 
        rule: allow
        port: '22'
        proto: tcp
    - ufw: 
        rule: allow
        port: '80'
        proto: tcp
    - ufw: 
        rule: allow
        port: '443'
        proto: tcp
    - name: Set firewall default policy
      ufw: 
        state: enabled
        policy: reject
    - name: Install libsemanage-python (needed by httpd_setrlimit) 
      yum: name=libsemanage-python state=latest  update_cache=yes
    - name: Install libselinux-python (needed by seport) 
      yum: 
        name: libselinux-python
        state: latest
        update_cache: yes
    - name: Install semanage, needed by seport 
      yum: 
        name: libsemanage-python 
        state: latest
        update_cache: yes
    - name: Install semanage, needed by seport 
      yum: 
        name: policycoreutils-python
        state: latest
        update_cache: yes
    - name: Install the selinux python module
      package: 
        name: nginx_python_selinux_pkgs
        state: present
      when: ansible_os_family == "Redhat"
    - name: Enable SELinux
      selinux:
        policy: targeted
        state: enforcing
    #- name: Allow listen on tcp port 8000
    #  seport:
    #    ports: 8000
    #    proto: tcp
    #    setype: http_port_t
    #    state: present
    - name: Change the httpd_t domain to permissive
      selinux_permissive:
        name: httpd_t
        permissive: true
    - name: Set SELinux boolean to allow nginx to set rlimit
      seboolean: 
        name: httpd_setrlimit
        state: yes
        persistent: yes
    - name: ensure nginx is at the latest version
      yum: name=nginx state=latest  update_cache=yes
    - name: install yum-utils
      yum: name=pygpgme state=latest  update_cache=yes
    - name: install pygpgme
      yum: name=pygpgme state=latest  update_cache=yes
    - name: Add imeyer_runit repository into a file
      yum_repository:
        name: imeyer_runit
        description: imeyer_runit repo
        file: imeyer_runit
        baseurl: https://packagecloud.io/imeyer/runit/el/6/$basearch
        repo_gpgcheck: yes
        gpgcheck: no
        gpgkey: https://packagecloud.io/imeyer/runit/gpgkey
        sslverify: yes
        sslcacert: /etc/pki/tls/certs/ca-bundle.crt
        metadata_expire: 300
        enabled: yes
    - name: Add imeyer_runit-source repository into a file
      yum_repository:
        name: imeyer_runit-source
        description: imeyer_runit repo
        file: imeyer_runit
        baseurl: https://packagecloud.io/imeyer/runit/el/6/SRPMS
        repo_gpgcheck: yes
        gpgcheck: no
        gpgkey: https://packagecloud.io/imeyer/runit/gpgkey
        sslverify: yes
        sslcacert: /etc/pki/tls/certs/ca-bundle.crt
        metadata_expire: 300
        enabled: yes
    - name: install runit
      yum: name=runit state=latest  update_cache=yes
    - name: copy the nginx config file
      copy:
        src: /vagrant/config/nginx.conf
        dest: /etc/nginx/nginx.conf
        owner: root
        group: root
        mode: 0644
    - name: create nginx ssl/tls certificates directory
      file:
        path: /etc/nginx/ssl
        state: directory
        mode: 0644
    - name: copy the private key
      copy:
        src: /vagrant/files/self-signed.key
        dest: /etc/nginx/ssl/application.key
        owner: root
        group: root
        mode: 0644
    - name: copy the certificate
      copy:
        src: /vagrant/files/self-signed.crt
        dest: /etc/nginx/ssl/application.crt
        owner: root
        group: root
        mode: 0640
    - name: restart Nginx
      service:
        name: nginx
        state: restarted
    - name: extracts application.zip into /vagrant/files/application
      unarchive:
        src: /vagrant/application.zip
        dest: /vagrant/files/
    - name: Ensure group "app" exists
      group:
        name: app 
        gid: 1000
        state: present
    - name: Add the 'app' user
      user:
        name: app
        comment: application user
        uid: 1000
        group: app 
        shell: /sbin/nologin
        state: present
    - name: Set privileges of /opt/application folder
      file:
        path: /opt/application/
        recurse: yes
        owner: app 
        group: app 
    - name: copy application run script
      copy:
        src: /vagrant/files/application/run
        dest: /opt/application/run
        owner: app
        group: app
        mode: 0755
    - name: copy application server.py script
      copy:
        src: /vagrant/files/application/server.py
        dest: /opt/application/server.py
        owner: app
        group: app
        mode: 0644
    - name: create /etc/runit directory
      file:
        path: /etc/runit/application/log
        state: directory
        mode: 0644
    - name: copy the applicaton run file
      copy:
        src: /vagrant/files/application_run
        dest: /etc/runit/application/run
        owner: root
        group: root
        mode: 0755
    - name: copy the applicaton log run file
      copy:
        src: /vagrant/files/log_run
        dest: /etc/runit/application/log/run
        owner: root
        group: root
        mode: 0744
    - name: create /etc/service directory
      file:
        path: /etc/service
        state: directory
        mode: 0755
    - name: Create a symbolic link
      file:
        src: /etc/runit/application
        dest: /etc/service/application
        owner: root
        group: root 
        state: link
    - name: Use alternative sv directory location
      runit:
        name: application
        state: started
        service_dir: /etc/service
    - name: start nginx
      service:
          name: nginx
          state: started

